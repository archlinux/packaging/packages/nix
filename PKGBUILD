# Maintainer: George Rawlinson <grawlinson@archlinux.org>
# Maintainer: Caleb Maclennan <caleb@alerque.com>

# NOTE:
# Since 2.20.0 the gc feature (--enable-gc) requires a patched bwd-gc or newer
# version, since this is a build time dependency we don't have yet we can't
# enable that feature. Can be re-enabled when we can supply bwd.
# c.f. https://github.com/NixOS/nix/pull/9512/files

pkgbase=nix
pkgname=(nix perl-nix)
pkgver=2.26.2
pkgrel=2
pkgdesc='A purely functional package manager'
arch=(x86_64)
url="https://nixos.org/nix"
license=(LGPL-2.1-only)
depends=(boost-libs libboost_context.so
         brotli libbrotlienc.so libbrotlidec.so
         curl libcurl.so
         editline libeditline.so
         gcc-libs
         glibc
         libarchive libarchive.so
         libcpuid libcpuid.so
         libgit2 libgit2.so
         libseccomp libseccomp.so
         libsodium
         lowdown liblowdown.so
         nix-busybox
         openssl libcrypto.so libsodium.so
         sqlite libsqlite3.so)
makedepends=(boost
             bzip2
             cmake
             gc
             git
             graphviz
             gtest
             jq
             meson
             nlohmann-json
             perl
             perl-dbd-sqlite
             rapidcheck librapidcheck.so
             toml11)
source=("$pkgbase::git+https://github.com/NixOS/nix.git#tag=${pkgver}?signed")
sha512sums=('dac7f9f4299a8f37377cdec4a531d651a4a9940a15329d614a43651bfed02474adb4a31385a60697426552a2545590d3a0d3cfe71e738d1e817479ac9ef3f8fa')
b2sums=('8a888092aee2ec4dae6877cbd15de991ac9994fbc91465dd049d6771396f2d5050d3fad166869e29aaaa02fc50b5523890a3e26a0a2caa18994a9c446369f0b3')
validpgpkeys=('B541D55301270E0BCF15CA5D8170B4726D7198DE') # Eelco Dolstra <edolstra@gmail.com>

build() {
	local meson_opts=(
		libstore:sandbox-shell=/usr/lib/nix/busybox
		bindings=true
		doc-gen=false
		unit-tests=false
	)
	arch-meson "$pkgbase" build ${meson_opts[@]/#/-D }
}

package_nix() {
	backup=("etc/$pkgname/$pkgname.conf")
	install="$pkgname.install"
	DESTDIR="$pkgdir" meson install -C build
	mv "$pkgdir/usr/lib/perl5" perl-nix
	install -Dm644 "$pkgbase/COPYING" "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_perl-nix() {
	depends=(nix)
	install -d "$pkgdir/usr/lib/perl5"
	mv perl-nix "$pkgdir/usr/lib/perl5/"
}
